#!/bin/bash
#User: Oze Farris <ofarris@hawaii.edu>
#Date: 02_01_2022
#Catnmouse2.sh
DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF
echo OK cat, I\'m thinking of a mumber from 1 to $THE_MAX_VALUE. Make a guess:

read guess

while :
do
   if [ $guess -lt 1 ]
   then 
      echo "You must enter a number that's >= 1."
   fi

   if [ $guess -gt $THE_MAX_VALUE ] 
   then 
      echo "You must enter a number that's <= $THE_MAX_VALUE."
   fi
   
   if [ $guess -eq $THE_NUMBER_IM_THINKING_OF ]
   then
      echo "You got me"
      echo "|\---/|"
      echo "| o_o |"
      echo  " \_^_/" 
      break
   fi
   
   if [ $guess -gt $THE_NUMBER_IM_THINKING_OF ] 
   then
      echo "No cat... the number I'm thinking of is smaller than $guess."
   else
      echo "No cat... the number I'm thinking of is larger than $guess."
   fi
 
   echo "Enter a new guess:"
   read guess
done

